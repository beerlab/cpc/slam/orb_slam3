ARG BASE_IMG=registry.gitlab.com/beerlab/cpc/utils/cps_ros_base_docker:latest

FROM ${BASE_IMG}

SHELL ["/bin/bash", "-ci"]

RUN apt update && \
    # Install build tools, build dependencies and python
    apt install --no-install-recommends -y \
	build-essential gcc g++ \
	cmake git libgtk2.0-dev pkg-config libavcodec-dev libavformat-dev libswscale-dev \
	libtbb2 libtbb-dev libjpeg-dev libpng-dev libtiff-dev \
    yasm libatlas-base-dev gfortran libpq-dev \
    libxine2-dev libglew-dev libtiff5-dev zlib1g-dev libavutil-dev libpostproc-dev libx11-dev tzdata \
	libglew-dev libboost-all-dev libssl-dev \
	libpcl-dev libogre-1.9-dev \
	libssl-dev libusb-1.0-0-dev libudev-dev pkg-config libgtk-3-dev \
	kmod \
	&& rm -rf /var/lib/apt/lists/*

# Set Working directory
WORKDIR /opt

RUN git clone https://github.com/stevenlovegrove/Pangolin.git && \
	cd Pangolin && mkdir build && cd build && \
	cmake .. -D CMAKE_BUILD_TYPE=Release && \ 
	make -j$(nproc) && \ 
	make install

# Realsense dependency
RUN mkdir -p /etc/apt/keyrings && \
	curl -sSf https://librealsense.intel.com/Debian/librealsense.pgp | sudo tee /etc/apt/keyrings/librealsense.pgp > /dev/null &&\
	echo "deb [signed-by=/etc/apt/keyrings/librealsense.pgp] https://librealsense.intel.com/Debian/apt-repo `lsb_release -cs` main" | \
	tee /etc/apt/sources.list.d/librealsense.list && \
	apt-get update && apt-get install -y librealsense2-utils librealsense2-dev\
	&& rm -rf /var/lib/apt/lists/*

RUN apt update && apt-get install -y libopencv-dev  libopencv-core4.2

WORKDIR /workspace/ros_ws
COPY . /workspace/ros_ws/src/ORB_SLAM3
COPY --from=registry.gitlab.com/beerlab/cpc/slam/orb_slam3:latest /workspace/ros_ws/build /workspace/ros_ws/build
RUN catkin build && echo "export ROS_PACKAGE_PATH=:/workspace/ros_ws/src/ORB_SLAM3/Examples_old/ROS/ORB_SLAM3:\$ROS_PACKAGE_PATH" >> ~/.bashrc

COPY --from=registry.gitlab.com/beerlab/cpc/slam/orb_slam3:latest /workspace/ros_ws/src/ORB_SLAM3/Examples_old/ROS/ORB_SLAM3/build /workspace/ros_ws/src/ORB_SLAM3/Examples_old/ROS/ORB_SLAM3/build
COPY --from=registry.gitlab.com/beerlab/cpc/slam/orb_slam3:latest /workspace/ros_ws/src/ORB_SLAM3/build /workspace/ros_ws/src/ORB_SLAM3/build

RUN cd src/ORB_SLAM3 && \
	sh build.sh && \
	sh build_ros.sh

CMD ["/bin/bash", "-ci", "source devel/setup.bash &&  cd src/ORB_SLAM3 &&rosrun ORB_SLAM3 Stereo_Inertial Vocabulary/ORBvoc.txt Examples/Stereo-Inertial/RealSense_D435i_save.yaml false false false"]
