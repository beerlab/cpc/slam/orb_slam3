# ORB-SLAM3-ODOM
This is a package for using the ORB SLAM3 as a complete robot sensing system for odometry estimation, localization and mapping.
## Running Stereo Localisation from DOCKER
### Maps
For localisation you need to put into your root of your machine in /orb_maps last.osa and optional last.bt

```
/orb_maps/last.osa
(optional) /orb_maps/last.bt
```
### Run

To run stereo-inertial:
```
docker compose -f docker-compose-stereo-local.yml up
```

Or if you have octomap run

```
docker compose -f docker-compose-stereo-local-octomap.yml up
```


### Топики нужные для работы!!!
/camera/imu

следующие 2 топика должны быть запущены с разрешением: 

width: 640
height: 480


/camera/infra1/image_rect_raw

для стерео +

/camera/infra2/image_rect_raw

Пример запуска roslaunch realsense2_camera:

```
roslaunch realsense2_camera rs_camera.launch enable_depth:=true depth_width:=640 depth_height:=480 depth_fps:=30 enable_infra1:=true infra_width:=640 infra_height:=480 enable_infra2:=true enable_gyro:=true enable_accel:=true unite_imu_method:=linear_interpolation filters:=pointcloud
```

## Running Mono


For run AR_drone, take:
```
docker compose -f docker-compose-ar-drone-mapping.yml up --pull always
```

## Dev


If you want to use local changes in ORB source files please use another docker-compose file.
```
docker compose -f docker-compose-bag.local.yml up --pull always
```

It mounts `Examples` and `Examples_old` directories inside docker container directly.

### Download the pre-build cache to quickly get started with development
If you want to download build cache in host computer use script:
```
./copy_build_from_registry.bash <IMAGE_TAG>
```


## Inside docker container
Перейти в папку ORB_SLAM3 (в dockere cd src/ORB_SLAM3)
### Команда запуска:
```
rosrun ORB_SLAM3 [MODE] [VOCABULARY PATH] [CONFIG PATH] [RECTIFY (Только в Stereo_Inertial)] [USE GUI] [LOCALISATION MODE]
```
<b>MODE</b> - режим работы (используются *Stereo_Inertial* и *Mono_Inertial*)

<b>VOCABULARY PATH</b> - путь к словарю (стандартно - *Vocabulary/ORBvoc.txt*)

<b>CONFIG PATH</b> - путь к конфигурационному файл с настройками для ORB

*Examples/Stereo-Inertial/RealSense_D435i_save.yaml* - это конфиг, который сохраняет карту в last.osa (ИСПОЛЬЗОВАТЬ ДЛЯ КАРТИРОВАНИЯ)

*Examples/Stereo-Inertial/RealSense_D435i_load.yaml* - это конфиг, который загружает последнюю last.osa карту (ИСПОЛЬЗОВАТЬ ДЛЯ ЛОКАЛИЗАЦИИ)

Если используете *Mono_Inertial*, то и путь *Examples/Monocular-Inertial/RealSense_D435i_save.yaml*

<b>RECTIFY (!ТОЛЬКО ДЛЯ РЕЖИМА Stereo_Inertial!)</b> - флаг регулирующий исправление дисторсии (принимает *true*/*false*)

<b>USE GUI</b> - флаг регулирующий gui (*true*/*false*)

<b>LOCALISATION MODE</b> - отвечает за режим:

*false* = картирование (с ним лучше сохранять карту)

*true* = локализация  (без загрузки карты работать не будет)

<b>Пример для Stereo_Inertial:</b>
```
rosrun ORB_SLAM3 Stereo_Inertial Vocabulary/ORBvoc.txt Examples/Stereo-Inertial/RealSense_D435i_save.yaml false true false
```

<b>Пример для Mono_Inertial:</b>
```
rosrun ORB_SLAM3 Mono_Inertial Vocabulary/ORBvoc.txt Examples/Monocular-Inertial/RealSense_D435i_save.yaml true false
```

### При картировании
Если теряется ничего страшного, после прохода в тех же местах после срабатывания VIVA 2 карты смержаться и будет одна большая. Для сохранения карты в конце нужно в GUI поставить галочку в step by step и нажать кнопку STOP. 
Если карта так себе и хочется заного достаточно ctrl+C.

### Для локализации на своей карте, нужно перейти в RealSense_D435i.yaml и в конце файла указать его название так (без .osa)
```
System.LoadAtlasFromFile: "last"
```
