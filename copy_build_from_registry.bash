#!/usr/bin/bash

if [ $# -ne 1 ]
  then
    echo "No arguments supplied. Please use: ./copy_build_from_registry.bash <IMAGE_TAG>"
    exit 1
fi

REMOTE_TAG=$1
echo "Get build cache for package from image: registry.gitlab.com/beerlab/cpc/slam/orb_slam3:$REMOTE_TAG"
id=$(docker create registry.gitlab.com/beerlab/cpc/slam/orb_slam3:$REMOTE_TAG)
docker cp $id:/workspace/ros_ws/src/ORB_SLAM3/Examples_old/ROS/ORB_SLAM3/build ./Examples_old/ROS/ORB_SLAM3/build
docker cp $id:/workspace/ros_ws/src/ORB_SLAM3/build ./build
docker rm -v $id